//
//  ContactUsViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 6/13/17.
//  Copyright © 2017 Qusay Bany Issa. All rights reserved.
//

import UIKit
import SwiftyXMLParser
import Alamofire

class ContactUsViewController: UIViewController {

    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var phoneLabel: UITextField!
    @IBOutlet weak var emailLabel: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
        self.title = "انضم لنا"
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func contactApiMethod() -> Void {
        
        
        
        if(emailLabel.text?.isEmpty)! || (nameLabel.text?.isEmpty)! || (phoneLabel.text?.isEmpty)! {
            
            MainClass().showAlertWithMessage(message: "الرجاء التأكد من تعبئة جميع الحقول المطلوبة", vc:self)
            
        }
    
        else if MainClass().isValidEmail(testStr: self.emailLabel.text!){
         
            let strURL = "http://cstc.me/shadi-career"
            let parameters:[String:String] = [
                "name": nameLabel.text!,
                "email": emailLabel.text!,
                "phone": phoneLabel.text!
            ]
            
            MainClass().showLoading(vc: self)
            
            
            Alamofire.request(strURL, method: .get, encoding: URLEncoding.default, headers: nil)
            .responseString{ (response) in
                if response.result.isSuccess {
                 
                    debugPrint(response.data)
                    MainClass().hideLoading()
                    
                                    let alertController = UIAlertController(title: "", message: "تم إرسال طلبك بنجاح", preferredStyle: UIAlertController.Style.alert)
                    
                                    let okAction = UIAlertAction(title: "حسناً", style: UIAlertAction.Style.default)
                                    {
                                        (result : UIAlertAction) -> Void in
                                        print("You pressed OK")
                                    }
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                    }
                     else if response.result.isFailure {
                       let error : Error = response.result.error!
                          print("error")
                      MainClass().hideLoading()
                }}

        }
        
        else {
            
            MainClass().showAlertWithMessage(message: "الرجاء التأكد من صيغة البريد الالكتروني", vc:self)
            
            
        }
        
        
    }
    @IBAction func sendButtonHandler(_ sender: Any) {
        
        if (emailLabel.text?.isEmpty)! || (phoneLabel.text?.isEmpty)! || (nameLabel.text?.isEmpty)!  {
            
            let alertController = UIAlertController(title: "", message: "تأكد من جميع الحقول المطلوبة", preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "حسناً", style: UIAlertAction.Style.default)
            {
                (result : UIAlertAction) -> Void in
                print("You pressed OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        
        else if MainClass().isValidEmail(testStr: self.emailLabel.text!){
            print("Validate EmailID")
            
            self.contactApiMethod()
            
        }
        else {
            
            let alertController = UIAlertController(title: "", message: "تأكد من صيغة البريد الالكتروني", preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "حسناً", style: UIAlertAction.Style.default)
            {
                (result : UIAlertAction) -> Void in
                print("You pressed OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
