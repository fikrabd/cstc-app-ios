//
//  CourseFormViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 11/7/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import SwiftyXMLParser
import ActionSheetPicker_3_0

class CourseFormViewController: UIViewController , UITextFieldDelegate, UITextViewDelegate{
    
    var courseTitle:String!
    var courseID:String!
    var index:NSInteger!
    var arrayData:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var sliderScrollView: UIScrollView!
    
    // note : all of these are text field not labels!
    @IBOutlet weak var courseTitleLabel: UILabel!
    
    @IBOutlet weak var agentNameLabel: UITextField!
    @IBOutlet weak var gehaLabel: UITextField!
    @IBOutlet weak var mobileNumLabel: UITextField!
    @IBOutlet weak var phoneLabel: UITextField!
    @IBOutlet weak var faxLabel: UITextField!
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var participantsLabel: UITextField!
    @IBOutlet weak var iterationsLabel: UITextField!
    
    @IBOutlet weak var countryLabel: UITextField!//
    @IBOutlet weak var cityLabel: UITextField!
    @IBOutlet weak var placeOfExecutionLabel: UITextField!
    @IBOutlet weak var servicesLabel: UITextField! //
    @IBOutlet weak var trainerClassificationLabel: UITextField! //
    
    
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        //scrollView.contentSize = CGSize(width: 375, height: 800)
        
        // hamdan
        var contentRect = CGRect.zero
        for view: UIView in self.sliderScrollView.subviews {
            contentRect = contentRect.union(view.frame)
        }
        
        // for user info padding
        //self.sliderScrollView.contentSize = contentRect.size;
        self.sliderScrollView.contentSize.height = contentRect.size.height + 100
        print("frame = \(self.sliderScrollView.frame.size) \n size =\(self.sliderScrollView.contentSize)")
  
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when   'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        agentNameLabel.resignFirstResponder()
        gehaLabel.resignFirstResponder()
        courseTitleLabel.resignFirstResponder()
        mobileNumLabel.resignFirstResponder()
        phoneLabel.resignFirstResponder()
        faxLabel.resignFirstResponder()
        emailLabel.resignFirstResponder()
        participantsLabel.resignFirstResponder()
        iterationsLabel.resignFirstResponder()
        countryLabel.resignFirstResponder()
        cityLabel.resignFirstResponder()
        servicesLabel.resignFirstResponder()
        trainerClassificationLabel.resignFirstResponder()
        
        self.view.endEditing(true)
    }
    
    
    func  validateFields() -> Bool{
        
        var flag = false
        if(agentNameLabel.text?.isEmpty)! || (courseTitleLabel.text?.isEmpty)! || (mobileNumLabel.text?.isEmpty)! || (phoneLabel.text?.isEmpty)! || (emailLabel.text?.isEmpty)! || (faxLabel.text?.isEmpty)! || (participantsLabel.text?.isEmpty)! || (faxLabel.text?.isEmpty)! || (iterationsLabel.text?.isEmpty)! || (countryLabel.text?.isEmpty)! || (cityLabel.text?.isEmpty)! || (servicesLabel.text?.isEmpty)! || (trainerClassificationLabel.text?.isEmpty)! {
            
            flag = true
        }
        
        return flag
    }


    @IBOutlet weak var backButtonHandler: UIButton!
    @IBAction func backButton(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func countryButtonHandler(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "اختر دولة", rows:   ["الأردن", "فلسطين", "لبنان", "العراق", "الكويت", "قطر", "الامارات", "البحرين", "عمان", "اليمن", "السعودية", "مصر", "ليبيا", "السودان", "الجزائر", "جيبوتي", "المغرب", "موريتانيا", "ماليزيا", "تونس", "اسبانيا", "تركيا"], initialSelection: 1, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            
            self.countryLabel.text = index! as? String
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender )

    }
    
    @IBOutlet weak var placeButtonHandler: UIButton!
    @IBAction func placeButton(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "اختر مكان التنفيذ", rows: ["داخل المنظمة", "فندق خمس نجوم", "فندق اربع نجوم"], initialSelection: 1, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            
            self.placeOfExecutionLabel.text = index! as? String
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender )
        
    }
    @IBAction func trainerButtonHandler(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "اختر تصنيف المدرب", rows:  ["خبير", "محترف", "ممتاز", "جيد جداً", "جيد"], initialSelection: 1, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            
            self.trainerClassificationLabel.text = index! as? String
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender )
        
    }
    @IBAction func servicesButtonHandler(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "اختر خدمات الضيافة", rows:  ["مرة واحدة", "مرتين", "غداء", "عشاء"], initialSelection: 1, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            
            self.servicesLabel.text = index! as? String
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender )
        
    }
    func getData()  {
        
        MainClass().showLoading(vc: self)
        
        let url = "http://cstc.me/shadi-request"
        let parametersa = [
            "course":"\(self.courseTitleLabel.text!)",
            "name":"\(self.agentNameLabel.text!)" ,
            "company":"\(self.gehaLabel.text!)" ,
            "mobile":"\(self.mobileNumLabel.text!)" ,
            "phone":"\(self.phoneLabel.text!)" ,
            "fax":"\(self.faxLabel.text!)" ,
            "email":"\(self.emailLabel.text!)" ,
            "partnumber":"\(self.participantsLabel.text!)" ,
            "redunduntcy":"\(self.iterationsLabel.text!)" ,///////////
            "country":"\(self.countryLabel.text!)" ,
            "city":"\(self.cityLabel.text!)" ,
            "tanfith":"\(self.placeOfExecutionLabel.text!)" ,//////////
            "services":"\(self.servicesLabel.text!)" ,
            "trainer":"\(self.trainerClassificationLabel.text!)" ]
        
        MainClass().showLoading(vc: self)
        
        
        Alamofire.request(url, method: .post, parameters: parametersa)
            .responseData { response in
                debugPrint("All Response Info: \(response)")
                
                if let data = response.result.value, let utf8Text = String(data: data, encoding: .utf8) {
                    
                    // parse xml document
                    let xml = try! XML.parse(utf8Text)
                    
                    print(xml.attributes.values)

                    MainClass().hideLoading()
                    
                    MainClass().showAlertWithMessage(message: "تم ارسال الطلب بنجاح", vc:self)
                    
                }
                else if let error = response.result.error {
                    MainClass().showAlertWithMessage(message: "حدث خطأ، حاول مرة أخرى", vc:self)
                }
            }
            
            .response { response in
                print("Request: \(response.request)")
                print("Response: \(response.response)")
                print("Error: \(response.error)")
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    
                    
                    
                }
            }
            
            .responseData { response in
                print(response.request)
                print(response.response)
                print(response.result)
                
            }
            
            .responseString { response in
                print("Success: \(response.result.isSuccess)")
                print("Response String: \(response.result.value)")
                
        }
    }

    @IBAction func sendFormButtonHandler(_ sender: Any) {
        
        if MainClass().isValidEmail(testStr: self.emailLabel.text!){
            print("Validate EmailID")
            
        var flag = self.validateFields()
            
            if flag == false {
            self.getData()
            }
            else {
                MainClass().showAlertWithMessage(message: "الرجاء التأكد من تعبئة جميع الحقول المطلوبة", vc:self)
            }
        }
        else{
            MainClass().showAlertWithMessage(message: "الرجاء التأكد من صيغة الايميل", vc:self)
        
            print("invalide EmailID")
        }
        
    }
    


override func viewDidLoad() {
    super.viewDidLoad()
    
//    self.navigationController?.isToolbarHidden = false
    // Do any additional setup after loading the view.
    
    courseTitleLabel.text = courseTitle
    
    agentNameLabel.delegate = self
    mobileNumLabel.delegate = self
    phoneLabel.delegate = self
    faxLabel.delegate = self
    emailLabel.delegate = self
    participantsLabel.delegate = self
    iterationsLabel.delegate = self
    countryLabel.delegate = self
    cityLabel.delegate = self
    servicesLabel.delegate = self
    trainerClassificationLabel.delegate = self
    
//    NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//    NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    
    
  
}

    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */

}
