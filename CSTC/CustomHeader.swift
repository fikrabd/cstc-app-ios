//
//  CustomHeader.swift
//  JawwalDaynamic
//
//  Created by LAith Khraisat on 7/20/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit
@objc protocol HeaderDelgate {
    
    @objc optional func actionHeaderRightButton()
    @objc optional func actionHeaderUsers()

}

@IBDesignable public class CustomHeader: UIView {
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var btnUsers: UIButton!
    
    var delegate: HeaderDelgate?
    
    class sharedInstance {
        static let sharedInstance = CustomHeader()
    }
    
    @IBAction func actionUsers(sender: AnyObject) {
        delegate?.actionHeaderUsers!()

    }
    
    /*
    override init(frame aRect: CGRect) {
        
        super.init(frame: aRect)
        
        var viewNew: UIView
       viewNew = loadViewFromNib()

        self.addSubview(viewNew)
        viewNew.frame = self.bounds
    }
    required public init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        addSubview(loadViewFromNib())

        
    }
     */
    class func initView(frame:CGRect) -> AnyObject? {
        
        
        
        let customView: CustomHeader = (Bundle.main.loadNibNamed("CustomHeader", owner: nil, options: nil)!.last)! as! CustomHeader
        
        _ = UIScreen.main.bounds
      //  let width = bounds.size.width
       // let height = bounds.size.height
      //  let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        customView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        
        
        return customView
        
        
    }

    @IBAction func actionSideOrBack(sender: AnyObject) {
        delegate?.actionHeaderRightButton!()
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomHeader", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }

    
    @IBInspectable var title: NSString = "title" {
        didSet {
            lblTitle.text = title as String
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
      // lblTitle.setFontAndSize()
        
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
