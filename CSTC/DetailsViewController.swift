//
//  DetailsViewController.swift
//  PagerDemo
//
//  Created by Qusay Mac on 10/7/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class DetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblHijri: UILabel!
    @IBOutlet weak var lblMilad: UILabel!
    var objHome:HomeObjects!
    @IBAction func back(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBOutlet weak var imgThumb: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let date1 = objHome.hijridate  as String
        let date2 = objHome.meladidate  as String
        lblHijri.text = date1.htmlToString
        lblLocation.text = objHome.location as String
        lblHours.text = objHome.hours as String
        lblMilad.text = date2.htmlToString
        lblTitle.text = objHome.title as String
        tblData.register(UINib(nibName: "DetailsCell", bundle: nil), forCellReuseIdentifier: "DetailsCell")

        //tblData.estimatedRowHeight = 431
        //tblData.rowHeight = UITableViewAutomaticDimension

        
        self.tblData.setNeedsLayout()
        self.tblData.layoutIfNeeded()

        
        Alamofire.request(objHome.link as String as String).responseImage { response in
            debugPrint(response)
            
            print(response.request)
            print(response.response)
            debugPrint(response.result)
            
            if let image = response.result.value {
                print("image downloaded: \(image)")
                self.imgThumb.image = image
            }
        }

    }
    override func viewDidAppear(_ animated: Bool) {
        self.tblData.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionRegister(_ sender: AnyObject) {
        
        self.performSegue(withIdentifier: "send", sender: self)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  6
    }
    /*
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cell: MainTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "DetailsCell") as? MainTableViewCell
        
        if cell == nil {
            tableView.register(UINib(nibName: "DetailsCell", bundle: nil), forCellReuseIdentifier: "DetailsCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell") as? MainTableViewCell
        }
        return cell.frame.size.height
    }
 */

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }


    /*
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
 */

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: MainTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "DetailsCell") as? MainTableViewCell
        
        if cell == nil {
            cell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell") as? MainTableViewCell
        }
        
        print(objHome.goul as String)
        print(objHome.tamaioz as String)
        print(objHome.quality as String)

        
        
        if indexPath.row == 0 {
            cell.lblTitle.text = "الفئة المستهدفة:"
            cell.lblDetails.text = objHome.sample as String
        }
        else
            if indexPath.row == 1 {
                cell.lblTitle.text = "الاهداف:"
                cell.lblDetails.text = objHome.goul as String
        }
        else
                if indexPath.row == 2 {
                    cell.lblTitle.text = "المحاور الرئيسية للدورة:"
                    cell.lblDetails.text = objHome.mahawir as String
        }
        else
                    if indexPath.row == 3 {
                        cell.lblTitle.text = "نظام الخصومات:"
                        cell.lblDetails.text = "نظام الخصومات"
        }
        else
                        if indexPath.row == 4 {
                            cell.lblTitle.text = "بماذا تتميز برامجنا التدريبية:"
                            cell.lblDetails.text = objHome.tamaioz as String
        }
        else
                            if indexPath.row == 5 {
                                cell.lblTitle.text = "معايير الجودة المطابقة:"
                                cell.lblDetails.text = objHome.quality as String
        }
 

        
        
        
        return cell
    }
    var index:NSInteger!
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let detailVC = segue.destination as! RegisterViewController
        
        detailVC.objHome  = self.objHome as HomeObjects
        

    }
    

}
