//
//  GeneralViewController.swift
//  Umniah
//
//  Created by LAith Khraisat on 8/2/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit
import AlamofireImage

class GeneralViewController: UIViewController,HeaderDelgate,RequestManagerDelegate {

    @IBOutlet weak var viewHeader: CustomHeader?
     var screenName : NSString = NSString ()
     var screenImg : NSString = NSString ()
     var isSideMenu : Bool = Bool ()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let viewHeader_ = viewHeader{
            let header:CustomHeader = CustomHeader.initView(frame: viewHeader_.frame) as! CustomHeader
            header.delegate = self
            header.lblTitle.text = screenName as String
            header.lblTitle.setFontAndSizeBold()
            header.imgMenu.image = UIImage(named: screenImg as String)
            viewHeader_.addSubview(header)
        }

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}
