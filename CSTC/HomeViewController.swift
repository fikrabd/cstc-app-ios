//
//  HomeViewController.swift
//  PagerDemo
//
//  Created by LAith Khraisat on 10/6/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
class HomeViewController: GeneralViewController,UITableViewDelegate,UITableViewDataSource {
    var arrayData:NSArray = []
    @IBOutlet weak var tblData: UITableView!
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        print("viewDidLoad")
        print("\(MainClass.webViewType)")
        
        
        // Do any additional setup after loading the view.
        if MainClass.webViewType.isEqual(to: "training") {
            let request:RequestManager = RequestManager()
            request.delegate = self
            request.getData(link: "http://cstc.me/?q=Field-training-xml", vc:self)
        }
        else  if MainClass.webViewType.isEqual(to: "plan") {
            let request:RequestManager = RequestManager()
            request.delegate = self
            
            
            let date = Date()
            let calendar = Calendar.current
            
            let year = calendar.component(.year, from: date)
            let month = calendar.component(.month, from: date)
            var newm = "\(month)"
            
            if (newm.count == 1){
                newm = "0\(newm)"
            }
            
//            let day = calendar.component(.day, from: date)
            
            var plus = "\(MainClass.trainingPlanIndex)"
            if (plus.count == 1){
                plus = "0\(plus)"
            }
            
            request.getData(link: "https://www.cstc.me/en/courses-xml/\(year)/\(plus)" as NSString, vc:self)
            
//            let strURL = "https://www.cstc.me/en/courses-xml/\(year)/\(newm)"
            
        }
            
     //   else {
      //      let request:RequestManager = RequestManager()
      //      request.delegate = self
      //      request.getData(link: "http://cstc.me/?q=courses-home-xml", vc:self)
        
     //   }
        else {
        let request:RequestManager = RequestManager()
        request.delegate = self
        request.getData(link: "http://cstc.me/?q=Field-training-xml", vc:self)
        }
        
    }
    func ResponseWithdataArray(dataArray: NSArray) {
        arrayData = dataArray
        print(dataArray)
        self.tblData.reloadData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
//        var numOfSections: Int = 0
//        if arrayData.count > 0
//        {
//            tableView.separatorStyle = .singleLine
//            numOfSections            = 1
//            tableView.backgroundView = nil
//        }
//        else
//        {
//            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
//            noDataLabel.text          = "لا توجد بيانات"
//            noDataLabel.textColor     = UIColor.black
//            noDataLabel.textAlignment = .center
//            tableView.backgroundView  = noDataLabel
//            tableView.separatorStyle  = .none
//        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrayData.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cell: MainTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell") as? MainTableViewCell
        
        
        if cell == nil {
            tableView.register(UINib(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "MainTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell") as? MainTableViewCell
        }
        return cell.frame.size.height
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: MainTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell") as? MainTableViewCell
        
        if cell == nil {
            tableView.register(UINib(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "MainTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell") as? MainTableViewCell
        }
        var obj:HomeObjects = HomeObjects()
        obj = arrayData.object(at: indexPath.row) as! HomeObjects
        cell.lblTitle.text = obj.title as String
        
        print(obj.title as String)
        print(obj.link as String)
        
        let imgLinkString = obj.link
        let  imgPath = NSURL(string: imgLinkString as String)
        
        Alamofire.request(imgLinkString as String).responseImage { response in
            debugPrint(response)
            
            print(response.request)
            print(response.response)
            debugPrint(response.result)
            
            if let image = response.result.value {
                print("image downloaded: \(image)")
                cell.imgThumb.image = image
            }
        }
        
        //    cell.imgThumb.setImageWithUrl(url: imgPath!, placeHolderImage: UIImage(named: "defult.png")!)
        return cell
    }
    var index:NSInteger!
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        
        if (MainClass.webViewType.isEqual(to: "training")) {
            
            self.performSegue(withIdentifier: "pushToCourses", sender: self)
        }
        else {
            self.performSegue(withIdentifier: "details", sender: self)
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "pushToCourses")
        {
            let innerCoursesVC : InnerCoursesViewController = segue.destination as! InnerCoursesViewController
            
                  var obj:HomeObjects = HomeObjects()
                obj = arrayData.object(at: index) as! HomeObjects
                let courseId : String = obj.id as String

                innerCoursesVC.courseID = String()
                innerCoursesVC.courseID = courseId
         
        }
        else {
            let detailVC = segue.destination as! DetailsViewController
            detailVC.objHome  = arrayData.object(at: index) as! HomeObjects
            print(detailVC.objHome.hijridate)
        }
        
        
    }
    
    
}
