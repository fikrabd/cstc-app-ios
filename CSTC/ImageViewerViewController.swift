//
//  ImageViewerViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 6/14/17.
//  Copyright © 2017 Qusay Bany Issa. All rights reserved.
//

import UIKit
import SwiftyXMLParser
import Alamofire
import SKPhotoBrowser

class ImageViewerViewController: UIViewController, SKPhotoBrowserDelegate {

    var dataArray:NSMutableArray = NSMutableArray()
    
    var id:String = String()
    var ttitle:String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //
        self.title = ttitle
        
        DispatchQueue.main.async {
            MainClass().showLoading(vc: self)
        }
        
        let strURL = "http://cstc.me/?q=photo-gallery-each-xml/\(Config.selectedImageId)"
        
        NetworkManager.getRequest(strURL, parameters: nil, onComplete: { response in
            
            print("response is : \(response) ")
            
            let xml = response as! SwiftyXMLParser.XML.Accessor
            
            for element in xml["nodes", "node"] {
                
                let link    = element["Image"].text
                
                self.dataArray.add(link)
                
            }
            
            MainClass().hideLoading()
            
            self.showImages()
            
        }) {
            (error) -> Void in
            print(error)
            
            MainClass().hideLoading()
        }
        
        
    }

    
    func showImages() -> Void {
        
        SKPhotoBrowserOptions.enableZoomBlackArea    = true  // default true
        SKPhotoBrowserOptions.enableSingleTapDismiss = true  // default false
        SKPhotoBrowserOptions.bounceAnimation        = true  // default false
        
        // 1. create URL Array
        var images = [SKPhoto]()
        let photo = SKPhoto.photoWithImageURL("http://cstc.me/sites/default/files/styles/large/public/dscn0189.preview.jpg?itok=iPTfgDGF")
        photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
        images.append(photo)
        
        // 2. create PhotoBrowser Instance, and present.
        let browser = SKPhotoBrowser(photos: images)
            browser.delegate = self
        browser.initializePageIndex(0)
        self.present(browser, animated: true, completion: {})
    }
    
    // MARK: - SKPhotoBrowserDelegate
    func didShowPhotoAtIndex(index: Int) {
        // when photo will be shown
        print("didShowPhotoAtIndex")
    }
    
    func willDismissAtPageIndex(index: Int) {
        // when PhotoBrowser will be dismissed
         print("willDismissAtPageIndex")
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func didDismissAtPageIndex(index: Int) {
        // when PhotoBrowser did dismissed
         print("didDismissAtPageIndex")
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
