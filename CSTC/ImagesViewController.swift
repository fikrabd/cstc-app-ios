//
//  ImagesViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 6/11/17.
//  Copyright © 2017 Qusay Bany Issa. All rights reserved.
//

import UIKit
import SwiftyXMLParser
import Alamofire
import SKPhotoBrowser



class ImagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,SKPhotoBrowserDelegate {

    var dataArray:NSMutableArray = NSMutableArray()
        var imagesArray:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.title = "معرض الصور"
        
        DispatchQueue.main.async {
           MainClass().showLoading(vc: self)
        }

        let strURL = "https://www.cstc.me/en/photo-gallery-xml"//http://cstc.me/?q=photo-gallery-xml"
        Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .responseJSON { (response) in
                if response.result.isSuccess {
                 
                    do {
                        if let array = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [[String: Any]] {
                            
                            for dict in array {
                                let id      = dict["nid"] as! String
                                let title   = dict["title"] as! String
                                let desc    = dict["body"] as! String
                                let pubDate = ""//dict["pubDate"].text as! String
                                let link    = dict["field_image"] as! String
                                
                                var objHome:HomeObjects = HomeObjects()
                                objHome.id = id as NSString
                                objHome.title = title as NSString
                                objHome.desc = desc as NSString
                                objHome.pubDate = ""//pubDate! as NSString
                                objHome.link = link as NSString
                                self.dataArray.add(objHome)
                                
                                self.myTableView.reloadData()
                                MainClass().hideLoading()
                            }
                        }
                        
                    }
                    catch {
                        print("Something went wrong")
                    }
                    
                }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view delegate and data source
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 250
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: MainTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell") as? MainTableViewCell
        
        if cell == nil {
            tableView.register(UINib(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "MainTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell") as? MainTableViewCell
        }
        var obj:HomeObjects = HomeObjects()
        obj = dataArray.object(at: indexPath.row) as! HomeObjects
        cell.lblTitle.text = obj.title as String
        
        
        
        let imgLinkString = obj.link
        let imgPath = NSURL(string: imgLinkString as String)
        
        Alamofire.request(imgLinkString as String).responseImage { response in
            
            
            
            if let image = response.result.value {
               
                cell.imgThumb.image = image
                cell.imgThumb.clipsToBounds = true
            }
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)

        
        var obj:HomeObjects = HomeObjects()
        obj = dataArray.object(at: indexPath.row) as! HomeObjects
        
        
        self.imagesArray = NSMutableArray()
        
        DispatchQueue.main.async {
            MainClass().showLoading(vc: self)
        }
        Config.selectedImageId = "\(obj.id)"
        let strURL = "https://www.cstc.me/en/photo-gallery-each-xml/\(obj.id)" ///"http://cstc.me/?q=photo-gallery-each-xml/
        print("images Url :  \(strURL)")
        
        
        
        Alamofire.request(strURL, method: .get, encoding: URLEncoding.default, headers: nil)
        .responseString{ (response) in
            if response.result.isSuccess {
             
                do{
                
                let xml = try! XML.parse(response.data!)
                

                    for element in xml["response", "item"] {

                        let link    = element["field_photo_gallery"].text

                        self.imagesArray.add(link)

                    }
                    
                    MainClass().hideLoading()
                    
                    self.showImages()
                
                }catch{
                    print("error parse")
                     MainClass().hideLoading()
                }
                
                
                }
                 else if response.result.isFailure {
                   let error : Error = response.result.error!
                      print("error")
                  MainClass().hideLoading()
                }
            
        }
        
        
        
        NetworkManager.getRequestXML(strURL, parameters: nil, onComplete: { response in
            
           print(response)
            
            
            
            
            
            
        }) {
            (error) -> Void in
            print("error")
            
            MainClass().hideLoading()
        }
        

        
      /*
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ImageViewerViewController") as? ImageViewerViewController {
            vc.id = obj.id as String
            vc.ttitle = obj.title as String
            if let navigator = navigationController {
                navigator.pushViewController(vc, animated: true)
            }
        }
        */
        

    }
   
    
    
    func showImages() -> Void {
        
        SKPhotoBrowserOptions.enableZoomBlackArea    = true  // default true
        SKPhotoBrowserOptions.enableSingleTapDismiss = true  // default false
        SKPhotoBrowserOptions.bounceAnimation        = true  // default false
        
        // 1. create URL Array
        var images = [SKPhoto]()
        
        for var i in (0..<imagesArray.count)
        {
            var link:String = String()
            link = imagesArray.object(at: i) as! String
            
            let photo = SKPhoto.photoWithImageURL(link)
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            images.append(photo)
            
        }
        
        // 2. create PhotoBrowser Instance, and present.
        let browser = SKPhotoBrowser(photos: images)
        browser.delegate = self
        browser.initializePageIndex(0)
        self.present(browser, animated: true, completion: {})
    }
    
    // MARK: - SKPhotoBrowserDelegate
    func didShowPhotoAtIndex(index: Int) {
        // when photo will be shown
        print("didShowPhotoAtIndex")
    }
    
    func willDismissAtPageIndex(index: Int) {
        // when PhotoBrowser will be dismissed
        print("willDismissAtPageIndex")

    }
    
    func didDismissAtPageIndex(index: Int) {
        // when PhotoBrowser did dismissed
        print("didDismissAtPageIndex")
    }
    

}
