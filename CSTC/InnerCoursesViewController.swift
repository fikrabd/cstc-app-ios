//
//  InnerCoursesViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 11/6/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import SwiftyXMLParser

extension UIButton {
    private struct AssociatedKeys {
        static var DescriptiveName = "KeyValue"
    }
    
    @IBInspectable var descriptiveName: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.DescriptiveName) as? String
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.DescriptiveName,
                    newValue as NSString?,
                    objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN
                )
            }
        }
    }
}

class InnerCoursesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var courseID:String!
    var index:NSInteger!
    var arrayData:NSMutableArray = NSMutableArray()
    @IBOutlet weak var tblData: UITableView!
    
    @IBAction func backButtonHandler(_ sender: Any) {
        
        self.navigationController?.isNavigationBarHidden = false
         self.navigationController?.popViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let link : String = "https://www.cstc.me/field-inner-xml/\(courseID!)"
        
        self.getData(link: link as NSString)
    }
    
    
    func getData(link:NSString)  {
        
       // MainClass().showLoading(vc : self)
        
        let url = link as String
        
        
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    debugPrint("All Response Info: \(response)")
                    do {
                        if let array = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [[String: Any]] {
                            print(array)
                            for dict in array {
                                let id      = dict["nid"] as! String
                                let title   = dict["title"] as! String
                               
                                var objHome:HomeObjects = HomeObjects()
                                    
                                objHome.title = title as NSString
                                objHome.id = id as NSString
                                self.arrayData.add(objHome)
                            }
                            self.tblData.reloadData()
                            MainClass().hideLoading()
                            
                        }
                        
                    }
                    catch {
                        print("Something went wrong")
                    }
                    
                }
        }
    }
    


    // MARK: - Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        if arrayData.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "لا توجد بيانات"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrayData.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80.0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: InnerTrainningViewCell! = tableView.dequeueReusableCell(withIdentifier: "InnerTrainningViewCell") as? InnerTrainningViewCell
        
        if cell == nil {
            tableView.register(UINib(nibName: "InnerTrainningViewCell", bundle: nil), forCellReuseIdentifier: "InnerTrainningViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "InnerTrainningViewCell") as? InnerTrainningViewCell
        }
        var obj:HomeObjects = HomeObjects()
        obj = arrayData.object(at: indexPath.row) as! HomeObjects
      //  cell.lblTitle.text = obj.title as String
        
        print(obj.title as String)
        print(obj.id as String)
        cell.infoLabel.text = (obj.title as String)
        
        cell.courseButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        cell.courseButton.tag = indexPath.row
        
        //let newI = i.replacingOccurrences(of: "GPS30", with: "")
        cell.courseButton.descriptiveName = "\(indexPath.row)" //newI use here
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        index = Int(indexPath.row)
        self.performSegue(withIdentifier: "pushToCourseForm", sender: self)
        
    }
   
    
    @objc func buttonAction(sender: UIButton!) {
        print(sender.descriptiveName!)
        
        index = Int(sender.descriptiveName!)
        self.performSegue(withIdentifier: "pushToCourseForm", sender: self)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let courseFormVC = segue.destination as! CourseFormViewController
        
        var obj:HomeObjects = HomeObjects()
        obj = arrayData.object(at: index) as! HomeObjects
        let coursetitle : String = obj.title as String

        courseFormVC.courseTitle  = String()
        courseFormVC.courseTitle  = coursetitle
        
        
    
        
    }
    
    
}
