//
//  InnerTrainningViewCell.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 11/6/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit

class InnerTrainningViewCell: UITableViewCell {

    @IBOutlet weak var courseButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
