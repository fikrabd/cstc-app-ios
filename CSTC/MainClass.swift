//
//  MainClass.swift
//  PlayFM
//
//  Created by LAith Khraisat on 6/7/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit
import AVFoundation

@available(iOS 10.0, *)
class MainClass: NSObject {
//    class mainClassShared {
        static let sharedInstance = MainClass()
//    }
    
    
      static  var spinnerActivity : MBProgressHUD = MBProgressHUD()
    
    
    static var msgInternetConnection = "Please check your internet connection"

    
    static var webViewType : NSString = NSString ()
    static var trainingPlanIndex : Int = 0

    static var sPage : NSString = NSString ()

    func vibrateField(fld:UITextField)  {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x:fld.center.x - 10, y:fld.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x:fld.center.x + 10, y:fld.center.y))
        fld.layer.add(animation, forKey: "position")
    }
    
    @available(iOS 10.0, *)
    func showNativeAlert(text:NSString)  {
        
        let alert = UIAlertController(title: "", message: text as String, preferredStyle: UIAlertController.Style.alert)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            switch action.style{
            case .default:
                
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))

    }
    func isAllDigits(str: String) -> Bool {
        let s:NSString = str as NSString
      if s.length == 14
            {
        let letters = NSCharacterSet.letters
        
        let phrase = str
        let range = phrase.rangeOfCharacter(from: letters)
        
        // range will be nil if no letters is found
        if range != nil {
            print("letters found")
            return false
        }
        else {
            print("letters not found")
            let nonNumbers = NSCharacterSet.decimalDigits
            
            if str.rangeOfCharacter(from: nonNumbers) != nil {
                return true
            }
            else {
                return false
            }
        }
        }
        else
        
        {
            return false
        }
    }

    func saveToUserDefaul(key:NSString,value:NSString)  {
        UserDefaults.standard.set(value as String, forKey: key as String)
    }
    func getFromUserDefault(key:NSString) -> String {
        
        let defaults = UserDefaults.standard
        
        if let name = defaults.string(forKey: key as String) {
            return name
        }
        else
        {
            return ""
        }
        
        //return NSUserDefaults.standardUserDefaults().stringForKey(key as String)!
    }
    

    func showLoading(vc: UIViewController)  {
        /*
        let alert = UIAlertController(title: nil, message: "الرجاء الانتظار...", preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50) ) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        vc.present(alert, animated: true, completion: nil)
*/
        
            MainClass.spinnerActivity.hide(animated: true)
        
            MainClass.spinnerActivity = MBProgressHUD.showAdded(to: vc.view, animated: true);
            
            MainClass.spinnerActivity.label.text = "";
            
            MainClass.spinnerActivity.detailsLabel.text = "الرجاء الإنتظار";
            
            MainClass.spinnerActivity.isUserInteractionEnabled = false;
        
        
        }
    
    func hideLoading()  {
        
/*
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.dismiss(animated: false, completion: nil)
*/
            MainClass.spinnerActivity.hide(animated: false)
    }
    func convertMinTo100(number:Int) -> Int {
        let x = number
        let s:NSString = "\(x)" as NSString
        let n = (s.length) - 1
        var last:NSString = "1"
        for _ in 0...n-2 {
            last = last.appending("0") as NSString
        }
        
        let lastInt:Int = Int(last as String)!
        return number / lastInt
    }
    
    func convertNumberTo100(used:Int, total:Int) -> Int {
        return used * 100 / total
    }

    func convertMax100(number:Int) -> Int {
        let x = number
        let s:NSString = "\(x)" as NSString
        let n = (s.length)-1
        var last:NSString = "1"
        for _ in 0...n-3 {
            last = last.appending("0") as NSString
        }
        let lastInt:Int = Int(last as String)!
        return number / lastInt
        
    }
    
    func setCurnerRadious(view:UIView)  {
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.cornerRadius = 5.0
    }

    func userAlreadyExist(kUsernameKey: String) -> Bool {
        return UserDefaults.standard.object(forKey: kUsernameKey) != nil
    }
    func pushLocalNotifcation(name:String, value:NSDictionary) {
        NotificationCenter.default
            .post(name: NSNotification.Name(rawValue: name), object: value)

    }
    func setLocalNotifcation(name:String) {

    }
    
    
    //hamdan
    func stringfunction(theString:String!) -> String {
        
        var newString : String
        
        if theString == nil {
            newString = " "
        }
        else {
            newString = theString
        }
        return newString
        
    }

    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }

    
    func showAlertWithMessage(message : String, vc : UIViewController!) {
        
        var alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        // Create the actions
        var okAction = UIAlertAction(title: "حسناً", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        vc.present(alertController, animated: true, completion: nil)
        
    }

}

import SystemConfiguration

extension UILabel {
    func setFontAndSize () {
        self.font =  UIFont(name: "Ebrima", size: 13)!
    }
}
extension UITextView {
    func setFontAndSize () {
        self.font =  UIFont(name: "Ebrima", size: 13)!
    }
}
extension UIButton {
    
    func setFontAndSize () {
        self.titleLabel!.font =  UIFont(name: "Ebrima", size: 13)!
    }
    
}
extension UILabel {
    func setFontAndSizeBold () {
        self.font =  UIFont(name: "Ebrima-Bold", size: 13)!
    }
}
extension UITextView {
    func setFontAndSizeBold () {
        self.font =  UIFont(name: "Ebrima-Bold", size: 13)!
    }
}
extension UIButton {
    func setFontAndSizeBold () {
        self.titleLabel!.font =  UIFont(name: "Ebrima-Bold", size: 13)!
    }
}

/*
extension String {
    
    var parseJSONString: AnyObject? {
        
        let data = self.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        var error: NSError?

        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            return NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers, error: &error)
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
    }
}*/

