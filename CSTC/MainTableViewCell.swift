//
//  MainTableViewCell.swift
//  JIS
//
//  Created by QusayBanyIssa on 8/20/16.
//  Copyright © 2016 QusayBanyIssa. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
    @IBOutlet weak var lblR: UILabel!

    @IBOutlet weak var lblSub: UILabel!
    @IBOutlet weak var lblMa: UILabel!
    @IBOutlet weak var lblFi: UILabel!
    @IBOutlet weak var lblS: UILabel!
    @IBOutlet weak var lblT: UILabel!
    @IBOutlet weak var lblF: UILabel!
    @IBOutlet weak var lblSTitle: UILabel!
    @IBOutlet weak var lblSFileDownload: UILabel!
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var txtDeails: UITextView!
    @IBOutlet var lblDetails: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
