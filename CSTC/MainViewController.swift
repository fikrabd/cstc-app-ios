//
//  MainViewController.swift
//  PagerDemo
//
//  Created by LAith Khraisat on 10/6/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet var viewDetails: UIView!
    
    @IBOutlet weak var btnHome: UIButton!
    
    @IBOutlet weak var sliderScrollView: UIScrollView!
    
    @IBOutlet weak var sliderView: UIView!
    
    weak var currentViewController: UIViewController?
    func cycleFromViewController(oldViewController: UIViewController, toViewController newViewController: UIViewController) {
        oldViewController.willMove(toParent: nil)
        self.addChild(newViewController)
        self.addSubview(subView: newViewController.view, toView:self.viewDetails!)
        newViewController.view.alpha = 0
        newViewController.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: {
            newViewController.view.alpha = 1
            oldViewController.view.alpha = 0
            },
                                   completion: { finished in
                                    oldViewController.view.removeFromSuperview()
                                    oldViewController.removeFromParent()
                                    newViewController.didMove(toParent: self)
        })
    }
    func addSubview(subView:UIView, toView parentView:UIView) {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|",
                                                                                 options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|",
                                                                                 options: [], metrics: nil, views: viewBindingsDict))
    }
    func loadViewToContainer(viewName:String)  {
        
        let newViewController = self.storyboard?.instantiateViewController(withIdentifier: viewName)
        newViewController!.view.translatesAutoresizingMaskIntoConstraints = false
        newViewController!.view.setNeedsLayout()
        newViewController!.view.setNeedsDisplay()
        
        
        
        /*
         if #available(iOS 9.0, *) {
         newViewController!.view.setNeedsFocusUpdate()
         } else {
         // Fallback on earlier versions
         }
         */
        
        self.cycleFromViewController(oldViewController: self.currentViewController!, toViewController: newViewController!)
        self.currentViewController = newViewController
        
    }

    @IBAction func actionHome(_ sender: AnyObject) {
        MainClass.webViewType = "home"
        loadViewToContainer(viewName: "home")

    }
    @IBAction func actionWebView(_ sender: AnyObject) {
        MainClass.webViewType = "about"
        loadViewToContainer(viewName: "webView")

        
    }
    @IBAction func actionPlan(_ sender: AnyObject) {
        MainClass.webViewType = "trainingPlan"
        
        loadViewToContainer(viewName: "trainingPlan")
        
    }
    @IBAction func actionTaining(_ sender: AnyObject) {
        MainClass.webViewType = "training"

        loadViewToContainer(viewName: "home")

    }
    @IBAction func actionEstsharat(_ sender: AnyObject) {
        MainClass.webViewType = "est"
        loadViewToContainer(viewName: "webView")

    }
    @IBAction func actionCall(_ sender: AnyObject) {
        MainClass.webViewType = "call"
        loadViewToContainer(viewName: "webView")

    }
    
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        //scrollView.contentSize = CGSize(width: 375, height: 800)
        
        // hamdan
        var contentRect = CGRect.zero
        for view: UIView in self.sliderScrollView.subviews {
            contentRect = contentRect.union(view.frame)
        }
        
        // for user info padding
        self.sliderScrollView.contentSize = contentRect.size;
        self.sliderScrollView.contentSize.height = 60
        
        print("frame = \(self.sliderScrollView.frame.size) \n size =\(self.sliderScrollView.contentSize)")

        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let appDelegate : AppDelegate = AppDelegate()
        appDelegate.mainViewController = self
        ////////////////////////////////////////////////////////////
        
        self.currentViewController = self.storyboard?.instantiateViewController(withIdentifier: "home")
        self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
        self.currentViewController!.view.setNeedsLayout()
        self.currentViewController!.view.setNeedsDisplay()
        self.addChild(self.currentViewController!)
        self.addSubview(subView: self.currentViewController!.view, toView: self.viewDetails)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //hamdan
    
    @IBAction func s1_buttonHandler(_ sender: AnyObject) {
        
        //https://plus.google.com/
        
        UIApplication.shared.openURL(URL(string: "https://plus.google.com/111076344477333324533")!)
        
    }
    @IBAction func s2_buttonHandler(_ sender: AnyObject) {
        
        //
        UIApplication.shared.openURL(URL(string: "https://www.youtube.com/user/cstctube")!)
        
    }
    @IBAction func s3_buttonHandler(_ sender: AnyObject) {
        
        //
        UIApplication.shared.openURL(URL(string: "https://twitter.com/CSTC_ME")!)
        
    }
    @IBAction func s4_buttonHandler(_ sender: AnyObject) {
        
        //
        UIApplication.shared.openURL(URL(string: "https://www.facebook.com/CSTC.ME")!)
    }
    
    @IBAction func b1_buttonHandler(_ sender: AnyObject) {
    }
    @IBAction func b2_buttonHandler(_ sender: AnyObject) {
    }
    @IBAction func b3_buttonHandler(_ sender: AnyObject) {
        
        //00966563333922
        if let url = URL(string: "tel://00966563333922") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
    @IBAction func b4_buttonHandler(_ sender: AnyObject) {
    }
    
    
    

}
