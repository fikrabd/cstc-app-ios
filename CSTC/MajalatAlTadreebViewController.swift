//
//  MajalatAlTadreebViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 6/13/17.
//  Copyright © 2017 Qusay Bany Issa. All rights reserved.
//

import UIKit
import SwiftyXMLParser
import Alamofire



class MajalatAlTadreebViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {

    var dataArray:NSMutableArray = NSMutableArray()
    var index:NSInteger!
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "مجالات التدريب"
        MainClass().showLoading(vc: self)
        let strURL = "https://www.cstc.me/en/Field-training-xml"// "http://cstc.me/?q=Field-training-xml"
        
        
        Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    debugPrint("All Response Info: \(response)")
                    do {
                        if let array = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [[String: Any]] {
                            print(array)
                            for dict in array {
                                let id      = dict["nid"] as! String
                                let title   = dict["title"] as! String
                                let desc    = dict["body"] as! String
                                let pubDate = ""//dict["pubDate"].text as! String
                                let link    = dict["field_image"] as! String
                                
                                var objHome:HomeObjects = HomeObjects()
                                objHome.id = id as NSString
                                objHome.title = title as NSString
                                objHome.desc = desc as NSString
                                objHome.pubDate = ""//pubDate! as NSString
                                objHome.link = link as NSString
                                self.dataArray.add(objHome)
                                
                                self.myCollectionView.reloadData()
                                MainClass().hideLoading()
                            }
                        }
                        
                    }
                    catch {
                        print("Something went wrong")
                    }
                    
                }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataArray.count
        
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! VideoCollectionViewCell
        
        
        var obj:HomeObjects = HomeObjects()
        obj = dataArray.object(at: indexPath.row) as! HomeObjects
        cell.videoTitle.text = obj.title as String
        
        print(obj.title as String)
        print(obj.link as String)
        
        let imgLinkString = obj.link
        let imgPath = NSURL(string: imgLinkString as String)
        
        Alamofire.request(imgLinkString as String).responseImage { response in
            debugPrint(response)
            
            //            print(response.request)
            //            print(response.response)
            //            debugPrint(response.result)
            
            if let image = response.result.value {
                print("image downloaded: \(image)")
                cell.videoImageView.image = image
                cell.videoImageView.clipsToBounds = true
                
            }
        }
        
        
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        index = indexPath.row
        var obj:HomeObjects = HomeObjects()
        obj = dataArray.object(at: indexPath.row) as! HomeObjects
        
        
        self.performSegue(withIdentifier: "pushToCourses", sender: self)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout else {
            return CGSize()
        }
        
        let widthAvailbleForAllItems =  (collectionView.frame.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right)
        
        let widthForOneItem = widthAvailbleForAllItems / 2 - flowLayout.minimumInteritemSpacing
        return CGSize(width: CGFloat(widthForOneItem), height: CGFloat(widthForOneItem))
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "pushToCourses")
        {
            let innerCoursesVC : InnerCoursesViewController = segue.destination as! InnerCoursesViewController
            
            var obj:HomeObjects = HomeObjects()
            obj = dataArray.object(at: index) as! HomeObjects
            let courseId : String = obj.id as String
            
            innerCoursesVC.courseID = String()
            innerCoursesVC.courseID = courseId
            
        }
        
    }
    

}
