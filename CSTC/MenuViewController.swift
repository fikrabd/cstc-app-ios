//
//  MenuViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 6/14/17.
//  Copyright © 2017 Qusay Bany Issa. All rights reserved.
//

import UIKit


class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var menuTitlesArray:NSMutableArray = NSMutableArray()
    var menuImagesArray:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        menuTitlesArray = ["الرئيسية","مجالات التدريب","الخطة التدريبية","البروفايل","اعداد الحقاذب التدريبية","الاختبارات","انضم إلينا","اتصل بنا","معرض الفيديو","معرض الصور"]
        
        menuImagesArray = ["ic_home_black_24dp.png","ico2.png","ico2.png","ico4.png","ic_setting_light.png","ico3.png","ico3.png","ico8.png","ic_local_movies_black_24dp.png","ic_photo_library_black_24dp.png"]
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  menuTitlesArray.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == 0) {
            
            // create a new cell if needed or reuse an old one
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! UITableViewCell

            return cell
        }
        else {
        var cell: MenuTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as? MenuTableViewCell
        
        if cell == nil {
            tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as? MenuTableViewCell
        }
        
        cell.titleLabel.text = menuTitlesArray[indexPath.row - 1] as? String
            
        cell.menuImageView.image = UIImage(named: menuImagesArray[indexPath.row - 1] as! String)
         return cell
        }

    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 0) {
            
            let ratio : Float = 0.39215686
            let screenWidth : Float =  270 //Float(UIScreen.main.bounds.size.width)

//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let rightVC = storyboard.instantiateViewController(withIdentifier: "rightVC")
//            let nav1 = storyboard.instantiateViewController(withIdentifier: "nav1")
//            let slideMenuController = SlideMenuController(mainViewController: nav1, rightMenuViewController:rightVC)
//        
//           let w = slideMenuController.rightContainerView.frame.size.width
            
            let hight : Float = ratio * screenWidth//Float(w)
            
            return CGFloat(hight)
        }
        else {
            
            return 50
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // tableView.deselectRow(at: indexPath, animated: true)
        
        self.slideMenuController()?.closeRight()
        
        if (indexPath.row == 0) {
            
            
            
        } else if (indexPath.row == 1) {

            
        } else if (indexPath.row == 2) {
            
            let mainVC = self.slideMenuController()?.mainViewController?.children.first as! NewMainViewController
            
            mainVC.b1(UIButton())
            
        } else if (indexPath.row == 3) {
            
            let mainVC = self.slideMenuController()?.mainViewController?.children.first as! NewMainViewController
            
            mainVC.b2(UIButton())
            
        } else if (indexPath.row == 4) {
            
            let mainVC = self.slideMenuController()?.mainViewController?.children.first as! NewMainViewController
            
            mainVC.b3(UIButton())
            
        } else if (indexPath.row == 5) {
            
            let mainVC = self.slideMenuController()?.mainViewController?.children.first as! NewMainViewController
            
            mainVC.b4(UIButton())
            
        } else if (indexPath.row == 6) {
            
            let mainVC = self.slideMenuController()?.mainViewController?.children.first as! NewMainViewController
            
            mainVC.b5(UIButton())
            
        } else if (indexPath.row == 7) {
            
            let mainVC = self.slideMenuController()?.mainViewController?.children.first as! NewMainViewController
            
            mainVC.b6(UIButton())
            
        } else if (indexPath.row == 8) {
            
            let mainVC = self.slideMenuController()?.mainViewController?.children.first as! NewMainViewController
            
            mainVC.b7(UIButton())
            
        } else if (indexPath.row == 9) {
            
            let mainVC = self.slideMenuController()?.mainViewController?.children.first as! NewMainViewController
            
            mainVC.b8(UIButton())
            
        } else if (indexPath.row == 10) {
            
            let mainVC = self.slideMenuController()?.mainViewController?.children.first as! NewMainViewController
            
            mainVC.b9(UIButton())
            
        }
        
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
