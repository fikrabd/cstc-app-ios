//
//  MessagesViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 11/12/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController {
    
    @IBOutlet weak var messagesTextView: UITextView!
    
    @IBAction func backButtonHandler(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getMessages() -> String {
        var contentOfFile : String!
        do {
            // get the documents folder url
            let documentDirectoryURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            // create the destination url for the text file to be saved
            let fileDestinationUrl = documentDirectoryURL.appendingPathComponent("messages.txt")

            do {
                
                do {
                    let mytext = try String(contentsOf: fileDestinationUrl)
                    print(mytext)   // "some text\n"
                    
                    contentOfFile = mytext
                } catch let error as NSError {
                    print("error loading contentsOf url \(fileDestinationUrl)")
                    print(error.localizedDescription)
                    contentOfFile = ""
                }
                
            }
        } catch let error as NSError {
            print("error getting documentDirectoryURL")
            print(error.localizedDescription)
        }
        
        return MainClass().stringfunction(theString: contentOfFile)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
       self.messagesTextView.text = self.getMessages()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
