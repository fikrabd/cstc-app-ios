//
//  NetworkManager.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 6/8/17.
//  Copyright © 2017 Qusay Bany Issa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyXMLParser


class NetworkManager: NSObject {
    
    
    class func getRequestXML(_ urlString: String, parameters params: [String: String]?, onComplete successBlock: @escaping (_ responseData: Any) -> Void, onError errorBlock: @escaping (_ error: Error?) -> Void) {
        
        Alamofire.request(urlString, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseString{ (response) in
                if response.result.isSuccess {
                  debugPrint("success Retrive")
                    }
                     else if response.result.isFailure {
                       let error : Error = response.result.error!
                        errorBlock(error)
                    }
                
            }
        
    }
    
    
    class func getRequest(_ urlString: String, parameters params: [String: String]?, onComplete successBlock: @escaping (_ responseData: Any) -> Void, onError errorBlock: @escaping (_ error: Error?) -> Void) {
        
        Alamofire.request(urlString, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    
                    debugPrint("All Response Info: \(response)")
                   
                }
                else if response.result.isFailure {
                    let error : Error = response.result.error!
                    errorBlock(error)
                }
        }
    }
    
    class  func postRequest(_ urlString: String, parameters params: [String: String], onComplete successBlock: @escaping (_ responseData: Any) -> Void, onError errorBlock: @escaping (_ error: Error?) -> Void) {
        
        
        Alamofire.request(urlString, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    
                    successBlock( (response.result.value as? [String:Any])!)
                    
                }
                
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    errorBlock(error)
                }
        }
    }
}
    
    
