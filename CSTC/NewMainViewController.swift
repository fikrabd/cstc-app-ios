//
//  NewMainViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 6/12/17.
//  Copyright © 2017 Qusay Bany Issa. All rights reserved.
//

import UIKit

class NewMainViewController: UIViewController {

    var titleString : String = String ()
    var link : String = String ()
    var webType : linkTypeEnum!
    
    @IBAction func menuAction(_ sender: Any) {
        
       self.slideMenuController()?.openRight()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.slideMenuController()?.closeRight()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func b1(_ sender: UIButton) {
         MainClass.webViewType = "home"
        self.performSegue(withIdentifier: "pushToMajalat", sender: self)
    }
    
    @IBAction func b2(_ sender: UIButton) {
        self.performSegue(withIdentifier: "pushToTrainnig", sender: self)
    
    }
    
    @IBAction func b3(_ sender: UIButton) {
        
       
        guard let url = URL(string: "https://www.cstc.me/sites/default/files/2019-12/profile.pdf") else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
        
        
//        self.link = "http://cstc.me/sites/default/files/CSTC%20Profile-compressed.pdf"
//        self.titleString = "البروفايل"
//        self.webType = linkTypeEnum.webURL
//        self.performSegue(withIdentifier: "pushToWebView", sender: self)
        
        
    }
    
    @IBAction func b4(_ sender: UIButton) {
        
        self.titleString = "اعداد الحقائب التدريبية"
        self.link = "prepare"
        self.webType = linkTypeEnum.localURL
        self.performSegue(withIdentifier: "pushToWebView", sender: self)
        
    }
    
    @IBAction func b5(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "", message: "غير متاح حالياً", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "حسناً", style: UIAlertAction.Style.default)
        {
            (result : UIAlertAction) -> Void in
            print("You pressed OK")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func b6(_ sender: UIButton) {
        
           self.performSegue(withIdentifier: "pushToJoin", sender: self)
    }
    
    @IBAction func b7(_ sender: UIButton) {
        
        self.link = "contact"
        self.titleString = "اتصل بنا"
        self.webType = linkTypeEnum.localURL
        self.performSegue(withIdentifier: "pushToWebView", sender: self)
    }
    
    @IBAction func b8(_ sender: UIButton) {
        
         self.performSegue(withIdentifier: "pushToVideos", sender: self)
    }
    
    @IBAction func b9(_ sender: UIButton) {
        
         self.performSegue(withIdentifier: "pushToImages", sender: self)
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "pushToWebView") {
            
            let webViewVC = segue.destination as! WebViewViewController
            webViewVC.webType = self.webType
            webViewVC.link = self.link
            webViewVC.titleString = self.titleString
            
        }
        
    }
 

}
