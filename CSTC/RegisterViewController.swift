//
//  RegisterViewController.swift
//  PagerDemo
//
//  Created by Qusay Mac on 10/8/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyXMLParser

class RegisterViewController: UIViewController,UIImagePickerControllerDelegate ,UINavigationControllerDelegate, UITextFieldDelegate{
    var objHome:HomeObjects!
    var imageBank : String!
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var bankImageView: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var fldNameAr: UITextField!
    @IBOutlet weak var fldNameEn: UITextField!
    @IBOutlet weak var fldQualification: UITextField!
    @IBOutlet weak var fldDgree: UITextField!
    @IBOutlet weak var fldEmployeer: UITextField!
    @IBOutlet weak var fldJob: UITextField!
    @IBOutlet weak var fldEmail: UITextField!
    @IBOutlet weak var fldJawal: UITextField!
    @IBOutlet weak var fldPhone: UITextField!
    @IBOutlet weak var fldFax: UITextField!
    @IBOutlet weak var fldCity: UITextField!
    
    
    @IBOutlet weak var sliderScrollView: UIScrollView!
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        //scrollView.contentSize = CGSize(width: 375, height: 800)
        
        // hamdan
        var contentRect = CGRect.zero
        for view: UIView in self.sliderScrollView.subviews {
            contentRect = contentRect.union(view.frame)
        }
        
        // for user info padding
//        self.sliderScrollView.contentSize = contentRect.size;
        self.sliderScrollView.contentSize.height = contentRect.size.height + 100
        print("frame = \(self.sliderScrollView.frame.size) \n size =\(self.sliderScrollView.contentSize)")
        
        
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        fldNameAr.resignFirstResponder()
        fldNameEn.resignFirstResponder()
        fldQualification.resignFirstResponder()
        fldDgree.resignFirstResponder()
        fldEmployeer.resignFirstResponder()
        fldJob.resignFirstResponder()
        fldEmail.resignFirstResponder()
        fldJawal.resignFirstResponder()
        fldPhone.resignFirstResponder()
        fldFax.resignFirstResponder()
        fldCity.resignFirstResponder()
        
        self.view.endEditing(true)
    }
    
    func  validateFields() -> Bool{
        
        var flag = false
        if(fldNameAr.text?.isEmpty)! || (fldNameEn.text?.isEmpty)! || (fldQualification.text?.isEmpty)! || (fldDgree.text?.isEmpty)! || (fldDgree.text?.isEmpty)! || (fldEmployeer.text?.isEmpty)! || (fldJob.text?.isEmpty)! || (fldEmail.text?.isEmpty)! || (fldJawal.text?.isEmpty)! || (fldPhone.text?.isEmpty)! || (fldFax.text?.isEmpty)! || (fldCity.text?.isEmpty)! {
            
            flag = true
        }
        
        return flag
    }
    

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when   'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    
    
    
    @IBAction func actionBank(_ sender: AnyObject) {
        
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            print("Button capture")
            
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    

//    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage

        
        bankImageView.image = selectedImage
        
        //   let NamImjVar = UIImage(named: "header_b_bg")
        let ImjDtaVar = selectedImage.jpegData(compressionQuality:1)//UIImageJPEGRepresentation(selectedImage, 1)
        
        MainClass().showLoading(vc :self)
        imageBank = ImjDtaVar?.base64EncodedString(options: [])
         MainClass().hideLoading()
       // print(imageBank)
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
//    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
//        self.dismiss(animated: true, completion: { () -> Void in
//            
//        })
//
//        
//    }
    
    
    @IBAction func actionSend(_ sender: AnyObject) {
        
            if MainClass().isValidEmail(testStr: self.fldEmail.text!){
                print("Validate EmailID")
                
                var flag = self.validateFields()
                
                if flag == false {
                    self.getData()
                }
                else {
                    MainClass().showAlertWithMessage(message: "الرجاء التأكد من تعبئة جميع الحقول المطلوبة", vc:self)
                }
            }
            else{
                MainClass().showAlertWithMessage(message: "الرجاء التأكد من صيغة الايميل", vc:self)
                
                print("invalide EmailID")
            }
            
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblName.text = objHome.title as String
        
        fldNameAr.delegate = self
        fldNameEn.delegate = self
        fldQualification.delegate = self
        fldDgree.delegate = self
        fldEmployeer.delegate = self
        fldJob.delegate = self
        fldEmail.delegate = self
        fldJawal.delegate = self
        fldPhone.delegate = self
        fldFax.delegate = self
        fldCity.delegate = self
//   
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var back: UIButton!

    @IBAction func backHandler(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getData()  {
        
        MainClass().showLoading(vc :self)
        
        let url = "http://cstc.me/shadi-plans"
        let parametersa = [
            "course":"\(self.lblName.text!)",
            "name":"\(self.fldNameAr.text!)" ,
            "name2":"\(self.fldNameEn.text!)" ,
            "moahel":"\(self.fldQualification.text!)" ,
            "taqassos":"\(self.fldDgree.text!)" ,
            "company":"\(self.fldEmployeer.text!)" ,
            "job":"\(self.fldJob.text!)" ,
            "email":"\(self.fldEmail.text!)" ,///////////
            "mobile":"\(self.fldJawal.text!)" ,
            "phone":"\(self.fldPhone.text!)" ,
            "fax":"\(self.fldFax.text!)" ,//////////
            "city":"\(self.fldCity.text!)" ,
            "imgname":"\(MainClass().stringfunction(theString: imageBank))" ]
        
        MainClass().showLoading(vc :self)
        
        
        Alamofire.request(url, method: .post, parameters: parametersa)
            .responseData { response in
                debugPrint("All Response Info: \(response)")
                
                if let data = response.result.value, let utf8Text = String(data: data, encoding: .utf8) {
                    
                    // parse xml document
                    let xml = try! XML.parse(utf8Text)
                    
                    print(xml.attributes.values)
                    
                    MainClass().hideLoading()
                    
                    MainClass().showAlertWithMessage(message: "تم ارسال الطلب بنجاح", vc:self)
                    
                    
                }
                else if let error = response.result.error {
                    MainClass().showAlertWithMessage(message: "حدث خطأ، حاول مرة أخرى", vc:self)
                }
                MainClass().hideLoading()
            }
            
            .response { response in
                print("Request: \(response.request)")
                print("Response: \(response.response)")
                print("Error: \(response.error)")
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    
                    
                    
                }
            }
            
            .responseData { response in
                print(response.request)
                print(response.response)
                print(response.result)
                
            }
            
            .responseString { response in
                print("Success: \(response.result.isSuccess)")
                print("Response String: \(response.result.value)")
                
        }
    }
  
    
    
    
    
    
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
}
