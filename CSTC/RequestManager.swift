//
//  RequestManager.swift
//  PagerDemo
//
//  Created by Qusay Mac on 10/7/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyXMLParser
import SwiftyJSON

@objc protocol RequestManagerDelegate {
    
    @objc optional func ResponseWithdataArray(dataArray:NSArray)
    
}

class RequestManager: NSObject,XMLParserDelegate {
    var strXMLData:String = ""
    var currentElement:String = ""
    var parser = XMLParser()
    var obj:HomeObjects = HomeObjects()
    var  homeArray = NSMutableArray()
    
    var delegate: RequestManagerDelegate?
    
    
    func getData(link:NSString, vc: UIViewController)  {
        MainClass().showLoading(vc : vc)
        
        print("\(link)")
        let url = link as String
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .response{ response in
                
               
                if response.error == nil {
                    self.homeArray = NSMutableArray ()
                    
                 do {
                    print(response.data)
                   let json = try JSON(data: response.data!)
                    
                    
                        print("222222")
                    print(json.count)
                   
                    for i in 0..<json.count{
                        let element = json[i]
                        print(element["nid"])
                        
                        let id      = element["nid"].stringValue
                        let title   = element["title"].stringValue
                        let desc    = element["field_features_our_programs"].stringValue
                        let pubDate  = ""
                        let link  = element["field_image"].stringValue
                        let quality  = element["field_quality"].stringValue
                        let meladidate  = element["field_date"].stringValue
                        let hijridate  = element["field_hijri_date"].stringValue
                        let month  = element["field_month"].stringValue
                        let sample  = element["field_sample"].stringValue
                        let location  = element["field_location"].stringValue
                        let hours  = element["field_hours_number2"].stringValue
                        let file  = element["field_file"].stringValue
                        let discount  = element["field_descount_system"].stringValue
                        let goul  = element["field_goul"].stringValue
                        
                        
                        let tamaioz  = element["field_features_our_programs"].stringValue
                        let mahawir  = element["field_course_type"].stringValue
                        let mkanplan  = element["field_location"].stringValue
                         let programtype  = element["field_course_type"].stringValue
                        
                        
                        var objHome:HomeObjects = HomeObjects()

                        objHome.id = id as NSString
                        objHome.title = title as NSString
                        objHome.desc = desc as NSString
                        objHome.pubDate = pubDate as NSString
                        objHome.link = link as NSString
                        objHome.quality = quality as NSString
                        objHome.meladidate = meladidate as NSString
                        objHome.hijridate = hijridate as NSString
                        objHome.month = month as NSString
                        objHome.sample = sample as NSString
                        objHome.mahawir = mahawir as NSString
                        objHome.location = location as NSString
                        objHome.tamaioz = tamaioz as NSString
                        objHome.hours = hours as NSString
                        objHome.mkanplan = mkanplan as NSString
                        objHome.file = file as NSString
                        objHome.discount = discount as NSString
                        objHome.programtype = programtype as NSString
                        objHome.goul = goul as NSString
                        
                        
                        self.homeArray.add(objHome)
                    }
                    MainClass().hideLoading()

                    self.delegate?.ResponseWithdataArray!(dataArray: self.homeArray)
                    

               
                 }catch{
                      print("Something went wrong")
                    }
                    
                    
                    
                    
                    
                }else{
                      print(response)
                }

                MainClass().hideLoading()
                
            }
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        currentElement=elementName;
        
        if(elementName=="node")
        {
            obj = HomeObjects ()
        }
        if(elementName=="nodes" )
        {
            homeArray = NSMutableArray ()
        }
        
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        print(currentElement)
        
        currentElement="";
        if(elementName=="node" )
        {
            homeArray.add(obj)
        }
        if(elementName=="nodes" )
        {
            print(homeArray)
            //end requests
            MainClass().hideLoading()

            delegate?.ResponseWithdataArray!(dataArray: homeArray)
        }
        
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        print(currentElement)
        if(currentElement=="title")
        {
            obj.title = string as NSString
        }
        else
            if(currentElement=="id")
            {
                obj.id = string as NSString
                
            }
            else
                if(currentElement=="desc")
                {
                    print(string)
                    var data :NSString = obj.desc
                    data = data.appending(string) as NSString
                    obj.desc = data
                    
                }
                else
                    if(currentElement=="pubDate")
                    {
                        obj.pubDate = string as NSString
                        
                    }
                    else
                        if(currentElement=="link")
                        {
                            obj.link = string as NSString
                            
                        }
                        else
                            if(currentElement=="quality")
                            {
                              //  obj.quality = string as NSString
                                var data :NSString = obj.quality
                                data = data.appending(string) as NSString
                                obj.quality = data

                            }
                            else
                                if(currentElement=="meladidate")
                                {
                                    obj.meladidate = string as NSString
                                    
                                }
                                else
                                    if(currentElement=="hijridate")
                                    {
                                        obj.hijridate = string as NSString
                                        
                                    }
                                    else
                                        if(currentElement=="month")
                                        {
                                            obj.month = string as NSString
                                            
                                        }
                                        else
                                            if(currentElement=="sample")
                                            {
                                               // obj.sample = string as NSString
                                                var data :NSString = obj.sample
                                                data = data.appending(string) as NSString
                                                obj.sample = data

                                            }
                                            else
                                                if(currentElement=="mahawir")
                                                {
                                                   // obj.mahawir = string as NSString
                                                    var data :NSString = obj.mahawir
                                                    data = data.appending(string) as NSString
                                                    obj.mahawir = data
 
                                                }
                                                else
                                                    if(currentElement=="location")
                                                    {
                                                        obj.location = string as NSString
                                                        
                                                    }
                                                    else
                                                        if(currentElement=="tamaioz")
                                                        {
                                                        //    obj.tamaioz = string as NSString
                                                            var data :NSString = obj.tamaioz
                                                            data = data.appending(string) as NSString
                                                            obj.tamaioz = data

                                                        }
                                                            
                                                            
                                                        else
                                                            if(currentElement=="hours")
                                                            {
                                                                obj.hours = string as NSString
                                                                
                                                            }
                                                                
                                                                
                                                            else
                                                                if(currentElement=="mkanplan")
                                                                {
                                                                  //  obj.mkanplan = string as NSString
                                                                    var data :NSString = obj.mkanplan
                                                                    data = data.appending(string) as NSString
                                                                    obj.mkanplan = data
 
                                                                }
                                                                    
                                                                    
                                                                else
                                                                    if(currentElement=="file")
                                                                    {
                                                                        obj.file = string as NSString
                                                                        
                                                                    }
                                                                        
                                                                        
                                                                    else
                                                                        if(currentElement=="discount")
                                                                        {
                                                                            obj.discount = string as NSString
                                                                            
                                                                        }
                                                                            
                                                                            
                                                                        else
                                                                            if(currentElement=="programtype")
                                                                            {
                                                                                obj.programtype = string as NSString
                                                                                
                                                                            }
                                                                            else
                                                                                if(currentElement=="goul")
                                                                                {
                                                                                    //obj.goul = string as NSString
                                                                                    var data :NSString = obj.goul
                                                                                    data = data.appending(string) as NSString
                                                                                    obj.goul = data

                                                                                    
        }
        
        
        
        
        
        
    }
    
    private func parser(parser: XMLParser, parseErrorOccurred parseError: NSError) {
        NSLog("failure error: %@", parseError)
    }
    
    
}
