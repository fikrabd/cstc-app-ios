//
//  TrainingCourcesViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 11/5/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit
import CarbonKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}



class TrainingCourcesViewController: UIViewController,CarbonTabSwipeNavigationDelegate {

    @IBOutlet weak var innerView: UIView!
    
    @IBAction func backButtonHandler(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        print("back")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        super.viewDidLoad()
        let items = [ "كانون الثاني", "شباط", "آذار", "نيسان", "أيار", "حزيران", "تموز", "آب", "أيلول", "تشرين الأول", "تشرين الثاني", "كانون الأول"]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        

        carbonTabSwipeNavigation.setNormalColor( UIColor(netHex:0x5B4A42), font: UIFont(name: "Al-Jazeera-Arabic", size: 16)!)
        carbonTabSwipeNavigation.setSelectedColor( UIColor(netHex:0x5B4A42), font: UIFont(name: "Al-Jazeera-Arabic", size: 16)!)
        carbonTabSwipeNavigation.setIndicatorColor( UIColor(netHex:0xF88C05))
        //colors
        
        
        //carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: self.innerView)
        
        
        let date = Date()
        let calendar = Calendar.current
        let month = calendar.component(.month, from: date)
        print("month = \(month)")
        
        carbonTabSwipeNavigation.currentTabIndex = UInt(month - 1)
        
    }
    
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        print("didMoveAt")
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        
         print("willMoveAt")
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
       
        print("viewControllerAt")
        MainClass.webViewType = "plan"
        MainClass.trainingPlanIndex = Int(index)+1
        
        
        
        
        
        
        let newViewController = self.storyboard?.instantiateViewController(withIdentifier: "home")
        
        return newViewController!
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
