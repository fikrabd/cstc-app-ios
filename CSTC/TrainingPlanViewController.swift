//
//  TrainingPlanViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 11/5/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit

class TrainingPlanViewController: UIViewController {


    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "الخطة التدريبية"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonHandler(_ sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    

    @IBAction func download_ViewPDF(_ sender: Any) {
        
        guard let url = URL(string: "https://www.cstc.me/sites/default/files/2019-12/plansV2.pdf") else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
