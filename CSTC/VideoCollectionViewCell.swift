//
//  VideoCollectionViewCell.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 6/13/17.
//  Copyright © 2017 Qusay Bany Issa. All rights reserved.
//

import UIKit

class VideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var videoTitle: UILabel!
}
