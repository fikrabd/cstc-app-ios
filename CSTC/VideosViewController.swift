//
//  VideosViewController.swift
//  PagerDemo
//
//  Created by Mohammad Hamdan on 6/13/17.
//  Copyright © 2017 Qusay Bany Issa. All rights reserved.
//

import UIKit
import SwiftyXMLParser
import Alamofire
import Kingfisher

class VideosViewController: UIViewController,  UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {

    var dataArray:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "معرض الفيديو"
    
        MainClass().showLoading(vc: self)
        let strURL = "https://www.cstc.me/en/video-xml"//http://cstc.me/?q=video-xml"
        Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    debugPrint("All Response Info: \(response)")
                    do {
                        if let array = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [[String: Any]] {
                            print(array)
                            for dict in array {
                                let id      = dict["nid"] as! String
                                let title   = dict["title"] as! String
                                let desc    = dict["body"] as! String
                                let pubDate = ""//dict["pubDate"].text as! String
                                var link    = dict["field_video_url_1"] as! String
                                link = link.replacingOccurrences(of: "\" alt=\"\" typeof=\"foaf:Image\" />", with: "")
                                link = link.replacingOccurrences(of: "<img src=\"", with: "https://www.cstc.me/")
                                let video   = dict["field_video_url"] as! String
                                
                                
                                
                                
                                var objHome:HomeObjects = HomeObjects()
                                objHome.id = id as NSString
                                objHome.title = title as NSString
                                objHome.desc = desc as NSString
                                objHome.pubDate = ""//pubDate! as NSString
                                objHome.link = link as NSString
                                objHome.video = video as NSString
                                self.dataArray.add(objHome)
                                
                                self.myCollectionView.reloadData()
                                DispatchQueue.main.async {
                                          MainClass().hideLoading()
                                }
                                                        
                            }
                        }
                        
                    }
                    catch {
                        print("Something went wrong")
                    }
                    
                }
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataArray.count
        
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! VideoCollectionViewCell
        
        
        var obj:HomeObjects = HomeObjects()
        obj = dataArray.object(at: indexPath.row) as! HomeObjects
        cell.videoTitle.text = obj.title as String
        
        
        let imgLinkString = obj.link
         
        let url : NSString = imgLinkString
        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        let searchURL : URL = NSURL(string: urlStr as String)! as URL
        
        print("image Link : \(searchURL)")
       cell.videoImageView?.kf.setImage(with: searchURL)
        
             return cell
        }
        

        
        
   
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        var obj:HomeObjects = HomeObjects()
        obj = dataArray.object(at: indexPath.row) as! HomeObjects
        

        let url = URL(string:  obj.video as String)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as! URL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url as! URL)
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout else {
            return CGSize()
        }
        
        let widthAvailbleForAllItems =  (collectionView.frame.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right)
        
        let widthForOneItem = widthAvailbleForAllItems / 2 - flowLayout.minimumInteritemSpacing
        return CGSize(width: CGFloat(widthForOneItem), height: CGFloat(widthForOneItem))
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
