//
//  WebViewViewController.swift
//  PagerDemo
//
//  Created by Qusay Mac on 10/8/16.
//  Copyright © 2016 Qusay Bany Issa. All rights reserved.
//

import UIKit

class WebViewViewController: UIViewController, UIWebViewDelegate{
    
    
    @IBOutlet weak var webData: UIWebView!
    @IBOutlet var progressView: UIProgressView!
    
    var titleString : String =  String()
    var link : String =  String()
    var webType : linkTypeEnum!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //navigation bar title
        self.title = titleString
        
        if (self.webType == .localURL) {
            
            webData.scalesPageToFit = false
            webData.loadRequest(NSURLRequest(url: NSURL(fileURLWithPath: Bundle.main.path(forResource: link, ofType: "html")!) as URL) as URLRequest)
            
        }
        else if (self.webType == .webURL) {
            
            let url = NSURL (string: link)
            let requestObj = URLRequest(url: url! as URL)
            webData.loadRequest(requestObj)
            webData.delegate=self
            webData.scalesPageToFit = true
            self.progressView.setProgress(0.0, animated: false)
 
        }
        
        
        //        if MainClass.webViewType.isEqual(to: "about") {
//            
//            webData.loadRequest(NSURLRequest(url: NSURL(fileURLWithPath: Bundle.main.path(forResource: "about_us", ofType: "html")!) as URL) as URLRequest)
//
//           // let url = Bundle.main.url(forResource: "privacy", withExtension:"html")
//           // webData.loadr
//        }
//        else
//            if MainClass.webViewType.isEqual(to: "est") {
//                
//                webData.loadRequest(NSURLRequest(url: NSURL(fileURLWithPath: Bundle.main.path(forResource: "consultant", ofType: "html")!) as URL) as URLRequest)
//                
//                // let url = Bundle.main.url(forResource: "privacy", withExtension:"html")
//                // webData.loadr
//        }
//            else
//                if MainClass.webViewType.isEqual(to: "call") {
//                    
//                    webData.loadRequest(NSURLRequest(url: NSURL(fileURLWithPath: Bundle.main.path(forResource: "contact", ofType: "html")!) as URL) as URLRequest)
//                    
//                    // let url = Bundle.main.url(forResource: "privacy", withExtension:"html")
//                    // webData.loadr
//        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        self.progressView.setProgress(0.1, animated: false)
        self.progressView.isHidden = false
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.progressView.setProgress(1.0, animated: true)
        self.progressView.isHidden = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: NSError?) {
        
        self.progressView.setProgress(1.0, animated: true)
    }
}
